﻿using Microsoft.Bot.Connector;
using Molly.Config;
using Newtonsoft.Json.Linq;
using Recipe;
using System;
using System.Collections.Generic;
using System.IO;

namespace Molly.Services
{
    public class RecipesSearchService : RecipeGenerationService
    {
       

        public List<Attachment> searchComplexRecipe(string dishName, string ingredients, string type, string diet, string cuisine)
        {
            string urlForComplexRecipe = "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/searchComplex?addRecipeInformation=true&fillIngredients=false&limitLicense=false&number=5&offset=3&query=" + dishName + "&ranking=1&includeIngredients=" + ingredients + "&type=" + type + "&diet=" + diet + "&cuisine=" + cuisine;
            return this.searchRecipe(urlForComplexRecipe);
        }

        public RecipeData getRecipeInformation(long recipeId)
        {
            RecipeData recipe = null;
            string urlForRecipeData = String.Format(FoodAndRecipeAPI.recipeInfoUrl, recipeId);
            var responseRecipeData = this.getHttpResponse(urlForRecipeData);
            string recipeData = new StreamReader(responseRecipeData.GetResponseStream()).ReadToEnd();

            if (recipeData != null && recipeData != "")
            {
                recipe = JToken.Parse(recipeData).ToObject<RecipeData>();
            }

            return recipe;
        }
    }
}