﻿using Microsoft.Bot.Connector;
using Molly.MessagesConstants;
using Newtonsoft.Json.Linq;
using Recipe;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Molly.Services
{
    public class RecipeGenerationService : RequestService
    {
        public  List<RecipeData> favRecipe { get; set; }

        protected string genereateAdditionalText(RecipeData recipe)
        {
            string cuisines = "";
            string vegetarian = "";
            if (recipe != null)
            {
                if ( recipe.cuisines != null && recipe.cuisines.Count > 0)
                {
                    string cuisinesList = "";
                    foreach ( string cuisine in recipe.cuisines)
                    {
                        cuisinesList += cuisine + ",";
                    }
                    cuisinesList = cuisinesList.Substring(0, cuisinesList.Length - 1);
                    cuisines = String.Format(InformationVisualizationText.SearchMessages.CuisineInfoText, cuisinesList);
                }
                if ( recipe.vegetarian)
                {
                    vegetarian = InformationVisualizationText.SearchMessages.VegetarianFlagText;
                }
            }

            return cuisines + " " + vegetarian;
        }

        protected List<CardAction> getCommonButtons(RecipeData recipe)
        {
            List<CardAction> commonButtons = new List<CardAction>();

            if (recipe != null)
            {
                if (recipe.spoonacularSourceUrl != null)
                {
                    CardAction seeItButton = new CardAction()
                    {
                        Value = recipe.spoonacularSourceUrl,
                        Type = "openUrl",
                        Title = InformationVisualizationText.SearchMessages.SeeItButtonText
                    };
                    commonButtons.Add(seeItButton);
                }

                if (recipe.id > 0)
                {
                    CardAction cookItButton = new CardAction()
                    {
                        Type = "imBack",
                        Title = InformationVisualizationText.StepByStepMessages.CookItButtonText,
                        Value = String.Format(InformationVisualizationText.StepByStepMessages.CookItButtonActionText, recipe.id, recipe.title)
                    };
                    commonButtons.Add(cookItButton);
                }
            }
            return commonButtons;
        }

        protected virtual List<CardAction> getButtons(RecipeData recipe)
        {
            List<CardAction> cardButtons = this.getCommonButtons(recipe);

            if (recipe != null)
            {
                bool isRecipeInFav = false;
                if ( favRecipe != null && favRecipe.Count > 0 )
                {
                    var entity = favRecipe.FirstOrDefault(e => e.id == recipe.id);

                    if (entity != null)
                    {
                        isRecipeInFav = true;
                    }
                }
                
                if ( recipe.id > 0 )
                {
                    if( !isRecipeInFav )
                    {
                        CardAction favButton = new CardAction()
                        {
                            Type = "imBack",
                            Title = InformationVisualizationText.FavouritesMessages.AddToFavButtonText,
                            Value = String.Format(InformationVisualizationText.FavouritesMessages.AddButtonActionText, recipe.id, recipe.title)
                        };
                        cardButtons.Add(favButton);
                    }else
                    {
                        CardAction removeButton = new CardAction()
                        {
                            Type = "imBack",
                            Title = InformationVisualizationText.FavouritesMessages.RemoveButtonText,
                            Value = String.Format(InformationVisualizationText.FavouritesMessages.RemoveButtonActionText, recipe.id, recipe.title)
                        };
                        cardButtons.Add(removeButton);
                    }
                    
                }
            }
            return cardButtons;
        }

        protected List<Attachment> generateRecipeCardsList(List<RecipeData> recipes)
        {
            List<Attachment> recipeCards = new List<Attachment>();

            if (recipes != null && recipes.Count > 0)
            {
                foreach (RecipeData recipe in recipes)
                {
                    string preparation = "";

                    if (recipe.readyInMinutes > 0)
                    {
                        TimeSpan ts = TimeSpan.FromMinutes(recipe.readyInMinutes);
                        string timeString = ts.ToString("hh':'mm");
                        preparation = String.Format( InformationVisualizationText.SearchMessages.PrepTimeText, timeString);
                    }

                    string info = genereateAdditionalText(recipe);
                    List<CardAction> cardButtons = this.getButtons(recipe);
                    HeroCard recipeCard = new HeroCard()
                    {
                        Title = recipe.title.Equals("") ? null : recipe.title,
                        Subtitle = preparation.Equals("") ? null : preparation,
                        Images = new List<CardImage> { new CardImage(recipe.image) },
                        Buttons = cardButtons == null? null : cardButtons,
                        Text = info.Equals("") ? null : info
                    };
                    Attachment cardAttachment = recipeCard.ToAttachment();
                    recipeCards.Add(cardAttachment);
                }
            }
            return recipeCards;
        }

        protected List<Attachment> searchRecipe(string url)
        {
            var response = this.getHttpResponse(url);
            string recipeList = new StreamReader(response.GetResponseStream()).ReadToEnd();
            if (recipeList != null && recipeList != "")
            {

                if(recipeList.Contains("results"))
                {
                    recipeList = recipeList.Replace("results", "recipes");
                }
                
                Recipes recipes = JToken.Parse(recipeList).ToObject<Recipes>();

                if (recipes != null)
                {
                    return this.generateRecipeCardsList(recipes.recipes);
                }
            }
            return null;
        }
    }
}