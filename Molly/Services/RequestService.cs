﻿using Molly.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace Molly.Services
{
    public class RequestService
    {
        public HttpWebResponse getHttpResponse(string url)
        {
            if (url != null)
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Accept = "application/json";
                request.Headers["X-Mashape-Key"] = FoodAndRecipeAPI.XMashapeKey;

                return (HttpWebResponse)request.GetResponse();
            }
            return null;
        }
    }
}