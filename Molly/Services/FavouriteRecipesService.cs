﻿using Microsoft.Bot.Connector;
using Molly.Config;
using Molly.MessagesConstants;
using Newtonsoft.Json.Linq;
using Recipe;
using System;
using System.Collections.Generic;
using System.IO;


namespace Molly.Services
{
    public class FavouriteRecipesService : RecipeGenerationService
    {
       

        public List<Attachment> generateFavouriteCards( List<RecipeData> recipes )
        {
            if (recipes != null)
            {
                return generateRecipeCardsList(recipes);
            }
            return null;
        }

        
        protected  override List<CardAction> getButtons(RecipeData recipe)
        {
            List<CardAction> cardButtons = null;
            if (recipe != null)
            {
               cardButtons =  base.getCommonButtons(recipe);

                CardAction removeButton = new CardAction()
                {
                    Type = "imBack",
                    Title = InformationVisualizationText.FavouritesMessages.RemoveButtonText,
                    Value = String.Format(InformationVisualizationText.FavouritesMessages.RemoveButtonActionText, recipe.id, recipe.title)
                };
               cardButtons.Add(removeButton);
            }
            return cardButtons;
        }
    }
}