﻿using Molly.Config;
using Molly.MessagesConstants;
using Newtonsoft.Json.Linq;
using StepByStepInstruction;
using System;
using System.IO;

namespace Molly.Services
{
    public class StepByStepCookingService : RequestService
    {
        public string getStepInformation(Step step)
        {
            string stepInformation = "";
     
            if( step != null )
            {
                string ingredientsText = "";
                if ( step.ingredients != null && step.ingredients.Count > 0 )
                {
                    foreach (Ingredient ingredient in step.ingredients)
                    {
                        ingredientsText += "-" + ingredient.name + "\n\n";
                    }
                }
                string equipmentText = "";
                if (step.equipment != null && step.equipment.Count > 0)
                {
                    foreach (Equipment equipment in step.equipment)
                    {
                        equipmentText += "-" + equipment.name + "\n\n";
                    }
                }

                if(step.number > 0 && step.step != null && !step.Equals(""))
                {
                    stepInformation = String.Format(MollyMessages.StepByStepMessages.StepInformationMessage, step.number, step.step);

                    if (!ingredientsText.Equals("") && !equipmentText.Equals(""))
                    {
                        stepInformation = String.Format(MollyMessages.StepByStepMessages.StepInformationFullMessage, step.number, ingredientsText, equipmentText, step.step);
                    }

                    if (!ingredientsText.Equals("") && equipmentText.Equals(""))
                    {
                        stepInformation = String.Format(MollyMessages.StepByStepMessages.StepInformationIngredientsMessage, step.number, ingredientsText, step.step);
                    }

                    if (ingredientsText.Equals("") && !equipmentText.Equals(""))
                    {
                        stepInformation = String.Format(MollyMessages.StepByStepMessages.StepInformationEquipmentMessage, step.number, equipmentText, step.step);
                    }
                }else
                {
                    stepInformation = MollyMessages.StepByStepMessages.NoStepFoundMessage; 
                }
            }
            return stepInformation;
        }

        public Instruction searchStepByStepCookingInstruction(long recipeId)
        {
            Instruction instruction = null;
            string urlForRecipeInstruction = String.Format(FoodAndRecipeAPI.recipeInstructionUrl, recipeId);
            var response = this.getHttpResponse(urlForRecipeInstruction);
            string steps = new StreamReader(response.GetResponseStream()).ReadToEnd();

            if (steps != null && !steps.Equals(""))
            {
                instruction = JToken.Parse(steps.Substring(1, steps.Length-2)).ToObject<Instruction>();
            }
            return instruction;
        }
    }
}