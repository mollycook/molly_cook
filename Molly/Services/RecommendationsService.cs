﻿using Microsoft.Bot.Connector;
using Molly.Config;
using Molly.MessagesConstants;
using Recipe;
using RecommendationsAPI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;

namespace Molly.Services
{
    public class RecommendationsService : RecipeGenerationService
    {
        private static string AccountKey = "a26beb4a3dc24857bdcb082404e84351";
        private const string BaseUri = "https://westus.api.cognitive.microsoft.com/recommendations/v4.0";
        private static RecommendationsApiWrapper recommender = null;
        private static string modelId = "0c18202e-3e38-4f1b-814d-5cfe9f3bfa11";
        long buildId = 0;
        private Object thisLock = new Object();

        public void uploadRecommendationsFilesAndBuildModel()
        {
            recommender = new RecommendationsApiWrapper(AccountKey, BaseUri);
            UploadData();
            buildId = TrainModel();
            
           
        }

        public List<long> requestForRecomm(string userId)
        {
            recommender = new RecommendationsApiWrapper(AccountKey, BaseUri);
            buildId = recommender.GetLastBuildId(modelId);
            //recommender.SetActiveBuild(modelId, buildId);
            if ( userId!= null && !userId.Equals("") && buildId > 0 )
            {
                return GetRecommendationsSingleRequest(recommender, modelId, buildId, userId);
            }
            return null;
        }

        public static void UploadData()
        {
            recommender.DeleteUsage(modelId);

            var resourcesDirCatalog= HttpContext.Current.Server.MapPath(@"~/Resources/catalog.csv");
            var catalogFile = new FileInfo(resourcesDirCatalog);
            recommender.UploadCatalog(modelId, catalogFile.FullName, catalogFile.Name);

            var resourcesDirUsage = HttpContext.Current.Server.MapPath(@"~/Resources/usage.csv");
            var usageFile = new FileInfo(resourcesDirUsage);
            recommender.UploadUsage(modelId, usageFile.FullName, usageFile.Name);
        }

        public static long TrainModel()
        {
            long buildId = -1;
            #region training
            // Trigger a recommendation build.
            string operationLocationHeader;

            buildId = recommender.CreateRecommendationsBuild(modelId, "Recommendation Build " + DateTime.UtcNow.ToString("yyyyMMddHHmmss"),
                                                                     enableModelInsights: false,
                                                                     operationLocationHeader: out operationLocationHeader);
          

            #endregion

            return buildId;
        }

        public static List<long> GetRecommendationsSingleRequest(RecommendationsApiWrapper recommender, string modelId, long buildId, string userId)
        {
            List<long> recommendedRecipesIds = new List<long>();
            var itemSets = recommender.GetUserRecommendations(modelId, buildId, userId, 3);
            if (itemSets.RecommendedItemSetInfo != null)
            {
                foreach (RecommendedItemSetInfo recoSet in itemSets.RecommendedItemSetInfo)
                {
                    foreach (var item in recoSet.Items)
                    {
                        recommendedRecipesIds.Add(Convert.ToInt64(item.Id));
                    }
                }
            }
            return recommendedRecipesIds;
        }

        public void writeNewDataInUsageFile(string user,long recipeId, string action)
        {
            lock (thisLock)
            {

                string fileName = $"usage.csv";

                var path = HttpContext.Current.Server.MapPath(@"~\Resources\" + fileName);
                if (!File.Exists(path))
                {
                    using (File.Create(path)) { }
                }
                using (StreamWriter userHistoryFile = File.AppendText(path))
                {
                    string userId = Regex.Replace(user, @"[^A-Za-z0-9]+", "");
                    userHistoryFile.WriteLine(userId + "," + recipeId + "," + DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss") + "," + action);
                }
            }
        }

        public void writeNewDataInCatalog(RecipeData recipeInfo)
        {
            lock (thisLock)
            {

                var pathCatalog = HttpContext.Current.Server.MapPath(@"~\Resources\catalog.csv");
                string[] lines = System.IO.File.ReadAllLines(pathCatalog);
                bool contains = false;

                foreach (string line in lines)
                {
                    if (line.StartsWith(recipeInfo.id.ToString()))
                    {
                        contains = true;
                        break;
                    }
                }
                if (!contains)
                {
                    using (StreamWriter catalogFile = File.AppendText(pathCatalog))
                    {
                        string cuisine1 = null;
                        string cuisine2 = null;
                        string cuisine3 = null;

                        string time = "";

                        if (recipeInfo.cuisines != null && recipeInfo.cuisines.Count > 0)
                        {
                            if (recipeInfo.cuisines.Count >= 3)
                            {
                                cuisine1 = ",Cuisine1=" + recipeInfo.cuisines[0];
                                cuisine2 = ",Cuisine2=" + recipeInfo.cuisines[1];
                                cuisine3 = ",Cuisine3=" + recipeInfo.cuisines[2];
                            }
                            else if (recipeInfo.cuisines.Count == 2)
                            {
                                cuisine1 = ",Cuisine1=" + recipeInfo.cuisines[0];
                                cuisine2 = ",Cuisine2=" + recipeInfo.cuisines[1];
                            }
                            else if (recipeInfo.cuisines.Count == 1)
                            {
                                cuisine1 = ",Cuisine1=" + recipeInfo.cuisines[0];
                            }

                        }

                        if (recipeInfo.readyInMinutes > 0)
                        {
                            time = ",Time=" + recipeInfo.readyInMinutes;
                        }

                        catalogFile.WriteLine(recipeInfo.id + "," + recipeInfo.title.Replace(",", "") + ",recipe" + cuisine1 + cuisine2 + cuisine3 + ",Vegetarian=" + recipeInfo.vegetarian + time);
                    }

                }
            }
        }

        public List<Attachment> searchRandomRecipes()
        {
            string urlForRandomRecipe = FoodAndRecipeAPI.randomRecipeUrl;
            return this.searchRecipe(urlForRandomRecipe);
        }

        public List<Attachment> generateRecommendationCards(List<RecipeData> recipes)
        {
            if (recipes != null)
            {
                return generateRecipeCardsList(recipes);
            }
            return null;
        }

    }
}