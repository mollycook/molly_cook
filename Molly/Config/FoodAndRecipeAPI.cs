﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Molly.Config
{
    public class FoodAndRecipeAPI
    {
        public const string XMashapeKey = "I7w824uiFDmshzBUJ9YRn1pXFZYFp14aBR1jsndZpBzGI95WrU";
        public const string randomRecipeUrl = "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/random?limitLicense=false&number=5&addRecipeInformation=true";
        public const string similarRecipeUrl = "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/{0}/similar";
        public const string recipeInfoUrl = "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/{0}/information";
        public const string recipeInstructionUrl = "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/{0}/analyzedInstructions?stepBreakdown=true";
    }
}