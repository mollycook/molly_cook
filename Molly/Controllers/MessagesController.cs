﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Bot.Connector;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using StepByStepInstruction;
using Molly.MessagesConstants;
using Molly.Services;
using Recipe;
using System.Linq;
using System.Threading;
using System.Web;

namespace Molly

{
   [BotAuthentication]
    public class MessagesController : ApiController
    {

        Instruction instruction = null;
        BotData conversationData { get; set; }
        List<long> recipesForCookingList = new List<long>();
        List<RecipeData> favouriteRecipes = new List<RecipeData>();

        private FavouriteRecipesService favouriteRecipesService = new FavouriteRecipesService();
        private StepByStepCookingService stepByStepCookingService = new StepByStepCookingService();
        private RecipesSearchService recipesSearchService = new RecipesSearchService();
        private RecommendationsService recommendationsService = new RecommendationsService() ;
        
        public async Task<HttpResponseMessage> Post([FromBody]Activity activity)
        {
            var response = Request.CreateResponse(HttpStatusCode.NoContent);
            StateClient stateClient = activity.GetStateClient();
            conversationData = await stateClient.BotState.GetUserDataAsync(activity.ChannelId, activity.From.Id);

       
            ConnectorClient connector = new ConnectorClient(new Uri(activity.ServiceUrl));
            Activity reply = activity.CreateReply();
            
            try
            {
                
                if (activity.Type == ActivityTypes.Message)
                {
                    if (activity.Text.ToLower().Contains(RecognitionIndicators.HelpMessages.HelpIndicator))
                    {

                        await HandleHelpMessage(activity, connector);
                    }

                    else if (activity.Text.ToLower().Contains(RecognitionIndicators.SearchMessages.DishIndicator) || activity.Text.ToLower().Contains(RecognitionIndicators.SearchMessages.IngredientsIndicator) || activity.Text.ToLower().Contains(RecognitionIndicators.SearchMessages.TypeIndicator) || activity.Text.ToLower().Contains(RecognitionIndicators.SearchMessages.DietIndicator) || activity.Text.ToLower().Contains(RecognitionIndicators.SearchMessages.CuisineIndicator))
                    {
                        await HandleSearchMessage(activity, connector);
                    }

                    else if (activity.Text.ToLower().Contains(RecognitionIndicators.FavouritesMessages.AddToFavIndicator))
                    {
                        await HandleAddToFavouriteMessage(activity, connector);
                    }
                    else if (activity.Text.ToLower().Contains(RecognitionIndicators.FavouritesMessages.RemoveRecipeIndicator))
                    {
                        await HandleRemoveFromFavouriteMessage(activity, connector);
                    }
                    else if (activity.Text.ToLower().Contains(RecognitionIndicators.FavouritesMessages.FavIndicator))
                    {
                        await HandleFavouriteRecipeMessage(activity, connector);
                    }

                    else if (activity.Text.ToLower().Contains(RecognitionIndicators.StepByStepMessages.StepByStepInstructionIndicator))
                    {
                        await HandleInstructionMessage(activity, connector);
                    }
                    else if (conversationData.GetProperty<bool>(RecognitionIndicators.RecommendationsMessages.RecommendationsInProcessIndicator))
                    {
                        if (activity.Text.ToLower().Contains(RecognitionIndicators.RecommendationsMessages.RandomIndicator) || activity.Text.ToLower().Contains(RecognitionIndicators.RecommendationsMessages.HistoryIndicator))
                        {
                            await HandleRecommendationBasedOnAnswerMessage(activity, connector);
                        }else
                        {

                            conversationData.SetProperty<bool>(RecognitionIndicators.RecommendationsMessages.RecommendationsInProcessIndicator, false);
                            await HandleRecommendationMessage(activity, connector);
                        }
                    }

                    else if (activity.Text.ToLower().Contains(RecognitionIndicators.RecommendationsMessages.RecommIndicator))
                    {
                        if (activity.Text.ToLower().Contains(RecognitionIndicators.RecommendationsMessages.RandomIndicator) || activity.Text.ToLower().Contains(RecognitionIndicators.RecommendationsMessages.HistoryIndicator))
                        {
                            await HandleRecommendationBasedOnAnswerMessage(activity, connector);
                        }else
                        {
                            await HandleRecommendationMessage(activity, connector);
                        }
                
                    }

                    else if (conversationData.GetProperty<bool>(RecognitionIndicators.StepByStepMessages.CookInProcessIndicator))
                    {
                        if (activity.Text.ToLower().Contains(RecognitionIndicators.StepByStepMessages.NextStepIndicator))
                        {
                            await HandleInstructionNextStepMessage(activity, connector);
                        }

                        if (activity.Text.ToLower().Contains(RecognitionIndicators.StepByStepMessages.PrevStepIndicator))
                        {
                            await HandleInstructionPrevStepMessage(activity, connector);
                        }

                        if (activity.Text.ToLower().Contains(RecognitionIndicators.StepByStepMessages.CurrentStepIndicator))
                        {
                            await HandleInstructionCurrentStepMessage(activity, connector);
                        }
                    }
                    else
                    {
                        reply.Text = null;
                        foreach (string greeting in RecognitionIndicators.GeneralMessages.GreetingIndicator)
                        {
                            if (activity.Text.ToLower().Contains(greeting))
                            {
                                reply.Text = MollyMessages.GeneralMessages.HelloMessage; 
                                await connector.Conversations.ReplyToActivityAsync(reply);
                                break;
                            }
                        }
                        if(reply.Text == null )
                        {
                            reply.Text = MollyMessages.GeneralMessages.ErrorMessage; 
                            await connector.Conversations.ReplyToActivityAsync(reply);
                        }
                    }
                }
                else
                {
                    await HandleSystemMessage(activity, connector);
                }

                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                await connector.Conversations.ReplyToActivityAsync(activity.CreateReply(MollyMessages.GeneralMessages.ExceptionMessage + e.Message));
                conversationData.SetProperty<bool>(RecognitionIndicators.StepByStepMessages.CookInProcessIndicator, false);
            }
            await stateClient.BotState.SetUserDataAsync(activity.ChannelId, activity.From.Id, conversationData);
            return response;
        }

        private async Task<Activity> HandleSearchMessage(Activity activity, ConnectorClient connector)
        {
            conversationData.SetProperty<bool>(RecognitionIndicators.RecommendationsMessages.RecommendationsInProcessIndicator, false);
            conversationData.SetProperty<bool>(RecognitionIndicators.StepByStepMessages.CookInProcessIndicator, false);
            var reply = activity.CreateReply();

            reply.Text = MollyMessages.SearchMessages.StartSearchingMessage;
            connector.Conversations.ReplyToActivity(reply);

            List<Attachment> cards = null;
            string[] sentences = Regex.Split(activity.Text, @"(\S.+?[.!?])(?=\s+|$)");

            string dishName = null;
            string ingredients = null;
            string cuisine = null;
            string type = null;
            string diet = null;

            foreach (string sentence in sentences)
            {
                int positionOfDish = sentence.ToLower().IndexOf(RecognitionIndicators.SearchMessages.DishIndicator);
                int positionOfIngredient = sentence.ToLower().IndexOf(RecognitionIndicators.SearchMessages.IngredientsIndicator);
                int positionOfCousine = sentence.ToLower().IndexOf(RecognitionIndicators.SearchMessages.CuisineIndicator);
                int positionOfType = sentence.ToLower().IndexOf(RecognitionIndicators.SearchMessages.TypeIndicator);
                int positionOfDiet = sentence.ToLower().IndexOf(RecognitionIndicators.SearchMessages.DietIndicator);

                if ( positionOfDish != -1 )
                {
                    dishName = sentence.Substring(positionOfDish + RecognitionIndicators.SearchMessages.DishIndicator.Length + 1);
                }
                if ( positionOfIngredient != -1 )
                {
                    ingredients = sentence.Substring(positionOfIngredient + RecognitionIndicators.SearchMessages.IngredientsIndicator.Length + 1);
                }
                if ( positionOfCousine != -1 )
                {
                    cuisine = sentence.Substring(positionOfCousine + RecognitionIndicators.SearchMessages.CuisineIndicator.Length + 1);
                }
                if ( positionOfType != -1 )
                {
                    type = sentence.Substring(positionOfType + RecognitionIndicators.SearchMessages.TypeIndicator.Length + 1);
                }
                if ( positionOfDiet != -1 )
                {
                    diet = sentence.Substring(positionOfDiet + RecognitionIndicators.SearchMessages.DietIndicator.Length + 1);
                }
            }

            try
            {
                recipesSearchService.favRecipe = conversationData.GetProperty<List<RecipeData>>(RecognitionIndicators.FavouritesMessages.FavListIndicator);
                cards = recipesSearchService.searchComplexRecipe(dishName, ingredients, type, diet, cuisine);
    
            } catch( Exception e )
            {
                cards = null;
            }

            if (cards != null && cards.Count > 0)
            {
                reply.Text = MollyMessages.SearchMessages.RecipesFoundMessage;
                reply.Recipient = activity.From;
                reply.Type = "message";
                reply.Attachments = cards;
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            }
            else
            {
                reply.Text = MollyMessages.SearchMessages.NothingFoundMessage; 
            }
            await connector.Conversations.ReplyToActivityAsync(reply);
            return null;
        }


        private async Task<Activity> HandleFavouriteRecipeMessage(Activity activity, ConnectorClient connector)
        {
            conversationData.SetProperty<bool>(RecognitionIndicators.RecommendationsMessages.RecommendationsInProcessIndicator, false);
            conversationData.SetProperty<bool>(RecognitionIndicators.StepByStepMessages.CookInProcessIndicator, false);

            var reply = activity.CreateReply();
            List<Attachment> cards = null;
            if (conversationData.GetProperty<List<RecipeData>>(RecognitionIndicators.FavouritesMessages.FavListIndicator) != null && conversationData.GetProperty<List<RecipeData>>(RecognitionIndicators.FavouritesMessages.FavListIndicator).Count > 0)
            {
                cards = favouriteRecipesService.generateFavouriteCards(conversationData.GetProperty<List<RecipeData>>(RecognitionIndicators.FavouritesMessages.FavListIndicator));
                if (cards != null && cards.Count > 0)
                {
                    reply.Text = MollyMessages.FavouritesMessages.FavListFoundMessage;
                    reply.Recipient = activity.From;
                    reply.Type = "message";
                    reply.Attachments = cards;
                    reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                }
                else
                {
                    reply.Text = MollyMessages.GeneralMessages.ExceptionMessage;
                }
            }
            else
            {
                reply.Text = MollyMessages.FavouritesMessages.FavListNotFoundMessage;
            }

            await connector.Conversations.ReplyToActivityAsync(reply);
            return null;
        }

        private async Task<Activity> HandleRemoveFromFavouriteMessage(Activity activity, ConnectorClient connector)
        {
            conversationData.SetProperty<bool>(RecognitionIndicators.RecommendationsMessages.RecommendationsInProcessIndicator, false);
            conversationData.SetProperty<bool>(RecognitionIndicators.StepByStepMessages.CookInProcessIndicator, false);

            var reply = activity.CreateReply();

            int start = activity.Text.IndexOf("(") + 1;
            int end = activity.Text.IndexOf(")", start);

            long recipeId = 0;
            if (start > 0 && end > 0)
            {
                string test = activity.Text.Substring(start, (end - start));
                recipeId = Convert.ToInt64(test);
            }
            if (recipeId > 0)
            {
                if (conversationData.GetProperty<List<RecipeData>>(RecognitionIndicators.FavouritesMessages.FavListIndicator) != null && conversationData.GetProperty<List<RecipeData>>(RecognitionIndicators.FavouritesMessages.FavListIndicator).Count > 0)
                {
                    favouriteRecipes = conversationData.GetProperty<List<RecipeData>>(RecognitionIndicators.FavouritesMessages.FavListIndicator);

                    var recipeToRemove = favouriteRecipes.Single(r => r.id == recipeId);
                    if (recipeToRemove != null)
                    {
                        favouriteRecipes.Remove(recipeToRemove);
                        conversationData.SetProperty<List<RecipeData>>(RecognitionIndicators.FavouritesMessages.FavListIndicator, favouriteRecipes);

                        reply.Text = MollyMessages.FavouritesMessages.RemovedMessage;
                        connector.Conversations.ReplyToActivity(reply);

                        HttpContext ctx = HttpContext.Current;
                        Thread t = new Thread(new ThreadStart(() =>
                        {
                            HttpContext.Current = ctx;
                            recommendationsService.writeNewDataInUsageFile(activity.From.Id, recipeId, "RemoveShopCart");
                            recommendationsService.uploadRecommendationsFilesAndBuildModel();
                        }));
                        t.Start();

                        await HandleFavouriteRecipeMessage(activity, connector);
                    }
                    else
                    {
                        reply.Text = MollyMessages.GeneralMessages.ExceptionMessage;
                        await connector.Conversations.ReplyToActivityAsync(reply);
                    }

                }
                else
                {
                    reply.Text = MollyMessages.GeneralMessages.ExceptionMessage;
                    await connector.Conversations.ReplyToActivityAsync(reply);
                }

            }
            else
            {
                reply.Text = MollyMessages.GeneralMessages.ExceptionMessage;
                await connector.Conversations.ReplyToActivityAsync(reply);
            }
            return null;
        }

        private async Task<Activity> HandleAddToFavouriteMessage(Activity activity, ConnectorClient connector)
        {
            conversationData.SetProperty<bool>(RecognitionIndicators.RecommendationsMessages.RecommendationsInProcessIndicator, false);
            conversationData.SetProperty<bool>(RecognitionIndicators.StepByStepMessages.CookInProcessIndicator, false);

            var reply = activity.CreateReply();

            int start = activity.Text.IndexOf("(") + 1;
            int end = activity.Text.IndexOf(")", start);

            long recipeId = 0;
            if (start > 0 && end > 0)
            {
                recipeId = Convert.ToInt64(activity.Text.Substring(start, (end - start)));
            }

            if (recipeId > 0)
            {
                if (conversationData.GetProperty<List<RecipeData>>(RecognitionIndicators.FavouritesMessages.FavListIndicator) != null && conversationData.GetProperty<List<RecipeData>>(RecognitionIndicators.FavouritesMessages.FavListIndicator).Count > 0)
                {
                    favouriteRecipes = conversationData.GetProperty<List<RecipeData>>(RecognitionIndicators.FavouritesMessages.FavListIndicator);
                }
                var entity = favouriteRecipes.FirstOrDefault(e => e.id == recipeId);
                if (entity != null)
                {
                    reply.Text = MollyMessages.FavouritesMessages.AlreadyAddedMessage;
                }
                else
                {
                    RecipeData recipeInfo = recipesSearchService.getRecipeInformation(recipeId);
                    favouriteRecipes.Add(recipeInfo);
                    conversationData.SetProperty<List<RecipeData>>(RecognitionIndicators.FavouritesMessages.FavListIndicator, favouriteRecipes);
                    reply.Text = MollyMessages.FavouritesMessages.AddedMessage;

                    HttpContext ctx = HttpContext.Current;
                    Thread t = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        recommendationsService.writeNewDataInCatalog(recipeInfo);
                        recommendationsService.writeNewDataInUsageFile(activity.From.Id, recipeId, "AddShopCart");
                        recommendationsService.uploadRecommendationsFilesAndBuildModel();
                    }));
                    t.Start();
                }
            }
            await connector.Conversations.ReplyToActivityAsync(reply);
            return null;
        }
    
        private async Task<Activity> HandleRecommendationBasedOnAnswerMessage(Activity activity, ConnectorClient connector)
        {

            conversationData.SetProperty<bool>(RecognitionIndicators.StepByStepMessages.CookInProcessIndicator, false);
            var reply = activity.CreateReply();
            reply.Text = MollyMessages.SearchMessages.StartSearchingMessage;
            connector.Conversations.ReplyToActivity(reply);
            List<Attachment> cards = null;
            List<long> recommendedRecipesIds = null;

            try
            {
                
                if (activity.Text.ToLower().Contains(RecognitionIndicators.RecommendationsMessages.RandomIndicator))
                {
                    cards = recommendationsService.searchRandomRecipes();

                    if (cards != null && cards.Count > 0)
                    {
                        reply.Text = MollyMessages.RecommendationsMessages.RandomRecipesMessage;
                        reply.Recipient = activity.From;
                        reply.Type = "message";
                        reply.Attachments = cards;
                        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                    }
                    else
                    {
                        reply.Text = MollyMessages.SearchMessages.NothingFoundMessage;
                    }
                }
                else if (activity.Text.ToLower().Contains(RecognitionIndicators.RecommendationsMessages.HistoryIndicator))
                {
                    string userId = Regex.Replace(activity.From.Id, @"[^A-Za-z0-9]+", "");
                    recommendedRecipesIds = recommendationsService.requestForRecomm(userId);
          
                    if ( recommendedRecipesIds != null && recommendedRecipesIds.Count > 0 )
                    {
                        List<RecipeData> recommRecipes = new List<RecipeData>();
                        foreach ( long  recipeId  in recommendedRecipesIds )
                        {
                            RecipeData recipeInfo = recipesSearchService.getRecipeInformation(recipeId);
                            recommRecipes.Add(recipeInfo);
                        }

                        if( recommRecipes != null && recommRecipes.Count > 0 )
                        {
                            cards = recommendationsService.generateRecommendationCards(recommRecipes);
                        }
                        
                        if (cards != null && cards.Count > 0)
                        {
                            reply.Text = MollyMessages.RecommendationsMessages.HistoryRecipesMessage;
                            reply.Recipient = activity.From;
                            reply.Type = "message";
                            reply.Attachments = cards;
                            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                        }
                        else
                        {
                            reply.Text = MollyMessages.GeneralMessages.ExceptionMessage;
                        }
                    }
                    else
                    {
                        reply.Text = MollyMessages.RecommendationsMessages.NoRecommendationsWasFoundMessage;
                    }
                }

               
            }
            catch (Exception e)
            {
                reply.Text = MollyMessages.GeneralMessages.ExceptionMessage;
            }
            await connector.Conversations.ReplyToActivityAsync(reply);
            return null;
        }

        private async Task<Activity> HandleRecommendationMessage(Activity activity, ConnectorClient connector)
        {
            conversationData.SetProperty<bool>(RecognitionIndicators.StepByStepMessages.CookInProcessIndicator, false);
            var reply = activity.CreateReply();
            conversationData.SetProperty<bool>(RecognitionIndicators.RecommendationsMessages.RecommendationsInProcessIndicator, true);
            reply.Text = MollyMessages.RecommendationsMessages.RecommendationChoiceMessage; 
            await connector.Conversations.ReplyToActivityAsync(reply);
        
            return null;
        }

        private async Task<Activity> HandleHelpMessage(Activity activity, ConnectorClient connector)
        {
            var reply = activity.CreateReply();

            reply.Text = MollyMessages.HelpMessages.MainHelpMessage;
            if (activity.Text.ToLower().Contains(RecognitionIndicators.HelpMessages.HelpSearchIndicator))
            {
                reply.Text = MollyMessages.HelpMessages.SearchHelpMessage; 
            }
            else if (activity.Text.ToLower().Contains(RecognitionIndicators.HelpMessages.HelpStepCookIndicator))
            {
                reply.Text = MollyMessages.HelpMessages.StepInstructionHelpMessage; 
            }
            else if (activity.Text.ToLower().Contains(RecognitionIndicators.HelpMessages.HelpRecomIndicator))
            {
                reply.Text = MollyMessages.HelpMessages.RecommendationHelpMessage; 
            }
            else if (activity.Text.ToLower().Contains(RecognitionIndicators.HelpMessages.HelpFavIndicator))
            {
                reply.Text = MollyMessages.HelpMessages.FavouritesHelpMessage;
            }
            await connector.Conversations.ReplyToActivityAsync(reply);
            return null;
        }


        private async Task<Activity> HandleInstructionMessage(Activity activity, ConnectorClient connector)
        {
            conversationData.SetProperty<bool>(RecognitionIndicators.RecommendationsMessages.RecommendationsInProcessIndicator, false);
            conversationData.SetProperty<bool>(RecognitionIndicators.StepByStepMessages.CookInProcessIndicator, true);
            var reply = activity.CreateReply();

            int start = activity.Text.IndexOf("(") + 1;
            int end = activity.Text.IndexOf(")", start);

            long recipeId = 0;
            if (start > 0 && end > 0)
            {
                recipeId = Convert.ToInt64(activity.Text.Substring(start, (end - start)));
            }
            if (recipeId > 0)
            {
                HttpContext ctx = HttpContext.Current;
                Thread t = new Thread(new ThreadStart(() =>
                {
                    HttpContext.Current = ctx;
                    RecipeData recipeInfo = recipesSearchService.getRecipeInformation(recipeId);
                    recommendationsService.writeNewDataInCatalog(recipeInfo);

                    recommendationsService.writeNewDataInUsageFile(activity.From.Id, recipeId, "Click");
                    recommendationsService.uploadRecommendationsFilesAndBuildModel();
                }));
                t.Start();
           
                reply.Text = MollyMessages.StepByStepMessages.RecipeIsChosenMessage; 
                connector.Conversations.ReplyToActivity(reply);

                reply.Text = MollyMessages.StepByStepMessages.WaitMessage;
                connector.Conversations.ReplyToActivity(reply);

                try
                {
                    instruction = stepByStepCookingService.searchStepByStepCookingInstruction(recipeId);
                }catch (Exception e)
                {
                    instruction = null;
                }
                

                if (instruction != null && instruction.steps.Count > 0)
                {
                    conversationData.SetProperty<Instruction>(RecognitionIndicators.StepByStepMessages.InstructionIndicator, instruction);
                    conversationData.SetProperty<int>(RecognitionIndicators.StepByStepMessages.CurrentStepNumberIndicator, 1);

                    reply.Text = String.Format(MollyMessages.StepByStepMessages.InstructionIntruductionMessage, instruction.steps.Count);
                    connector.Conversations.ReplyToActivity(reply);

                    reply.Text = stepByStepCookingService.getStepInformation(instruction.steps[0]);

                }
                else
                {
                    reply = activity.CreateReply(MollyMessages.StepByStepMessages.InstructionNotFoundMessage);
                    conversationData.SetProperty<bool>(RecognitionIndicators.StepByStepMessages.CookInProcessIndicator, false);
                }
            }
            else
            {
                reply = activity.CreateReply(MollyMessages.GeneralMessages.ExceptionMessage );
                conversationData.SetProperty<bool>(RecognitionIndicators.StepByStepMessages.CookInProcessIndicator, false);
            }
            await connector.Conversations.ReplyToActivityAsync(reply);
            return null;
        }

        private async Task<Activity> HandleInstructionNextStepMessage(Activity activity, ConnectorClient connector)
        {
            var reply = activity.CreateReply();
            instruction = conversationData.GetProperty<Instruction>(RecognitionIndicators.StepByStepMessages.InstructionIndicator);
            int currentStep = conversationData.GetProperty<int>(RecognitionIndicators.StepByStepMessages.CurrentStepNumberIndicator);

            if (instruction != null)
            {
                if ( currentStep == instruction.steps.Count )
                {
                    reply.Text = MollyMessages.StepByStepMessages.LastStepErrorMessage;
                }
                if (currentStep > 0 && currentStep < instruction.steps.Count)
                {
                    currentStep++;
                    conversationData.SetProperty<int>(RecognitionIndicators.StepByStepMessages.CurrentStepNumberIndicator, currentStep);
                    if (currentStep == instruction.steps.Count)
                    {
                        reply.Text = MollyMessages.StepByStepMessages.LastStepNotificationMessage;
                        connector.Conversations.ReplyToActivity(reply);
                    }
                    reply.Text = stepByStepCookingService.getStepInformation(instruction.steps[currentStep - 1]);
                }

            }

            await connector.Conversations.ReplyToActivityAsync(reply);
            return null;
        }

        private async Task<Activity> HandleInstructionPrevStepMessage(Activity activity, ConnectorClient connector)
        {
            var reply = activity.CreateReply();

            int currentStep = conversationData.GetProperty<int>(RecognitionIndicators.StepByStepMessages.CurrentStepNumberIndicator);

            if (currentStep == 1)
            {
                reply.Text = MollyMessages.StepByStepMessages.FirstStepErrorMessage;
            }

            if (currentStep > 1)
            {
                currentStep--;
                conversationData.SetProperty<int>(RecognitionIndicators.StepByStepMessages.CurrentStepNumberIndicator, currentStep);

                instruction = conversationData.GetProperty<Instruction>(RecognitionIndicators.StepByStepMessages.InstructionIndicator);
                if (instruction != null)
                {
                    reply.Text = stepByStepCookingService.getStepInformation(instruction.steps[currentStep - 1]);
                }
            }
            

            await connector.Conversations.ReplyToActivityAsync(reply);
            return null;
        }

        private async Task<Activity> HandleInstructionCurrentStepMessage(Activity activity, ConnectorClient connector)
        {
            var reply = activity.CreateReply();
            int currentStep = conversationData.GetProperty<int>(RecognitionIndicators.StepByStepMessages.CurrentStepNumberIndicator);
            if (currentStep > 0)
            {
                instruction = conversationData.GetProperty<Instruction>(RecognitionIndicators.StepByStepMessages.InstructionIndicator);
                if (instruction != null)
                {
                    string stepInfo = stepByStepCookingService.getStepInformation(instruction.steps[currentStep - 1]);
                    reply = activity.CreateReply(stepInfo);
                }
            }
            await connector.Conversations.ReplyToActivityAsync(reply);
            return null;
        }

        private async Task<Activity> HandleSystemMessage(Activity message, ConnectorClient connector)
        {
            if (message.Type == ActivityTypes.ContactRelationUpdate)
            {
                IContactRelationUpdateActivity update = message;
                if (update.Action.ToLower() == ContactRelationUpdateActionTypes.Add)
                {
                    var reply = message.CreateReply();
                    reply.Text = MollyMessages.GeneralMessages.IntroductionMessage;
                    await connector.Conversations.ReplyToActivityAsync(reply);
                }
              
            }
            return null;
        }
    }
}