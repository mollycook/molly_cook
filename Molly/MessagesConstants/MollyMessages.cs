﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Molly.MessagesConstants
{
    public class MollyMessages
    {

        public static class GeneralMessages
        {
            public const string ErrorMessage = "Sorry, I do not understand you! :^) Please type help to receive more information!";
            public const string HelloMessage = "Hello (hi) How can I help you?";
            public const string IntroductionMessage = "Hello (hi) My name is Molly. I will be your personal cooking assistant. Please, type 'help' to know more about me!";
            public const string ExceptionMessage = "Oups..something went wrong :S";
        }

        public static class SearchMessages
        {
            public const string StartSearchingMessage = "Start searching...(wait) Please wait a second!";
            public const string RecipesFoundMessage = "Here are some recipes, enjoy it! :)";
            public const string NothingFoundMessage = "Oups! Nothing was found (shake) Please try again! Our type 'help' for more information";
        }

        public static class StepByStepMessages
        {
            public const string RecipeIsChosenMessage = "Mmm...yummy :P Perfect choice! Let us cook together!";
            public const string WaitMessage = "Wait a second please...(wait)";
            public const string InstructionIntruductionMessage = "Here is an instruction. It has **{0} steps** in sum. \n\n---\n\nYou can switch between steps using “*prev*”/”*next*” keywords or ask for current step using “current” keyword. For more detailed help please type “*help with step by step cooking*”.";
            public const string InstructionNotFoundMessage = "Sorry (shake) Requested instruction was not found!";
            public const string LastStepErrorMessage = "It was the last step! (talktothehand)";
            public const string LastStepNotificationMessage = "Notice that it is the last step!";
            public const string FirstStepErrorMessage = "It is the first step! (talktothehand)";
            public const string NoStepFoundMessage = "Error.No step was found! (shake)";
            public const string StepInformationMessage = "**Step {0}.**\n\n{1}";
            public const string StepInformationFullMessage = "**Step {0}.**\n\n---\n\n**Ingredients**\n\n{1}\n\n---\n\n**Equipment**\n\n{2}\n\n---\n\n{3}";
            public const string StepInformationEquipmentMessage = "**Step {0}.**\n\n---\n\n**Equipment**\n\n{1}\n\n---\n\n{2}";
            public const string StepInformationIngredientsMessage = "**Step {0}.**\n\n---\n\n**Ingredients**\n\n{1}\n\n---\n\n{2}";
        }

        public static class RecommendationsMessages
        {
            public const string RecommendationChoiceMessage = "Do you want to receive the random recipes recommendations or recommendations based on your own history?";
            public const string RandomRecipesMessage = "Here are my favourite recipes! Hope you will enjoy them too! (inlove)";
            public const string HistoryRecipesMessage = "Here are some recipes which are based on your history. Enjoy! 	:)";
            public const string NoRecommendationsWasFoundMessage = "It was not found any recommendations for you (shake)";
        }

        public static class HelpMessages
        {
            public const string MainHelpMessage = "**Help instuction **  \n\n---\n\nHere are provided the possible actions divided by sections. \n\nType what section instruction you want to receive. \n\nPossible sections are: \n\n -Recipe search\n\n -Step by step cooking\n\n -Recommendations\n\n -Favourites \n\n---\n\n **Example** \n\n -help with recipe search \n\n -help with step by step cooking \n\n -help with recommendations \n\n -help with favourites";
            public const string SearchHelpMessage = "**Here is recipe search instruction. Hope it helps!** \n\nRecipe search is possible by following filters: dish name, ingredients, cuisine, type of the recipe and diet. \n\nPossible values are: \n\n -Cuisines: \n\n *african, chinese, japanese, korean, vietnamese, thai, indian, british, irish, french, italian, mexican, spanish, middle eastern, jewish, american, cajun, southern, greek, german, nordic, eastern european, caribbean, or latin american* \n\n -Type of the recipe: \n\n *main course, side dish, dessert, appetizer, salad, bread, breakfast, soup, beverage, sauce, or drink*\n\n -Diet: \n\n *pescetarian, lacto vegetarian, ovo vegetarian, vegan, paleo, primal, and vegetarian* \n\n---\n\n**Rules for formatting:** \n\n -Ingredients should be presented as list of products which is comma separate.\n\n -For the filter recognition, please use following indicators: \n\n*Dish* – for dish name \n\n*Ingredients* – for ingredients list \n\n*Diet* – for diet filter \n\n*Type* – for type of the recipe \n\n*Cuisine* – for cuisine. \n\n -Each filter request should represent the separate sentence. Sentence should be started from indicator + „:” mark. \n\n -All filters for one request should be written in one message. \n\n---\n\n **Examples** \n\n -Dish: pasta. Ingredients: chicken, broccoli. \n\n -Type: main course. Diet: vegetarian. \n\n -Type: salad. Cuisine: japanese.";
            public const string StepInstructionHelpMessage = "**Here is step by step cooking instruction. Hope it helps!** \n\nBefore you start step by step cooking, make sure that recipe search was done and results were received. \n\n Press '*cook it*' button on recipe card and function cook with Molly would be launched. \n\nYou can:\n\n -Get **next** step – write '*next*'/'*next step*' and Molly should provide the next step for you! \n\n -Get **previous** step –write '*previous*'/ '*previous step*' and Molly should provide the previous step for you. \n\n -Get **current** step – write '*current step*'/ '*current*' and Molly should send the current step for you. \n\n---\n\n Notice, if you make a new recipe search, the recipe instruction will be reset. You can ask for help at any time, without the resetting the current state.";
            public const string RecommendationHelpMessage = "**Here is instruction for recommendations. Hope it helps!** \n\nYou can ask for recommendations of two different types: based on your own **history** or just for **random** recipes. \n\nFor asking the recommendations please type something like this: \n\n -*Molly, please recommend me something.* \n\n -*Please, give me recommendations.* \n\n It could be any phrase with word starting with: “recomm”. \n\n---\n\nAfter that you could choose what kind of recommendation you would like to receive. Type history or random, or any phrase with one of these words. \n\nMoreover, you could directly request recommendation by its type. \n\nType phrase with combinations with two words: word starting with '*recomm*' and '*history*' or '*random*'. \n\n**For example**\n\n -Molly, please give me recommendation based on my history. \n\n -Molly, please give me the random recommendations. \n\n After the request you will receive recipe cards. Enjoy them! \n\n---\n\n Notice that recipes which you add to your favourites have the most priority on searching recommendations for you!";
            public const string FavouritesHelpMessage = "**Here is instruction for favourites. Hope it helps!** \n\nAfter searching was made, five recipes cards are provided to according to your requests. On recipe card there is a button '*add to favourites*'. Press it, and recipe would be added to favourites list. \n\nTo see your favourites, type something with word: '*fav*'.\n\n**Example**\n\n -Molly, please show me my favourites list. \n\n---\n\n Also, you could remove recipe from your favourites list. Just press button '*remove*' on recipe card.";

        }

        public static class FavouritesMessages
        {
            public const string FavListFoundMessage = "Here are your favourites recipes! (inlove)";
            public const string FavListNotFoundMessage = "Your favourite list is empty! (shake)";
            public const string RemovedMessage = "Recipe was removed successfully!";
            public const string AlreadyAddedMessage = "This recipe is already in your favourites! (talktothehand)";
            public const string AddedMessage = "Recipe was added to favourites! (inlove)";
        }
    }
}