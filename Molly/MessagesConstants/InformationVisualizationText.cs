﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Molly.MessagesConstants
{
    public class InformationVisualizationText
    {
        public static class SearchMessages
        {
            public const string CuisineInfoText = "Just try a delicacy of {0} cuisine!";
            public const string VegetarianFlagText = "If you are a vegetarian, this recipe is for you!";
            public const string PrepTimeText = "Preparation time: {0} hours";
            public const string SeeItButtonText = "See it";
        }

        public static class StepByStepMessages
        {
            public const string CookItButtonText = "Cook it";
            public const string CookItButtonActionText = "<meta recipeId=\'({0})\'/> I want to cook {1} !";
        }

            public static class FavouritesMessages
        {
            public const string RemoveButtonText = "Remove from favourites";
            public const string AddToFavButtonText = "Add to favourites";
            public const string AddButtonActionText = "<meta favRecipe=\'({0})\'/> Add to favourites {1} ";
            public const string RemoveButtonActionText = "<meta removeRecipe=\'({0})\'/> Remove recipe: {1} ";
        }

    }
}