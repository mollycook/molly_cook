﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Molly.MessagesConstants
{
    public class RecognitionIndicators
    {
        public static class GeneralMessages
        {
            public static readonly string[] GreetingIndicator = { "hello", "good morning", "good afternoon", "good evening", "hallo", "hi", "hey", "yo", "welcome", "glad to see you" };
        }

        public static class SearchMessages
        {
            public const string DishIndicator = "dish";
            public const string IngredientsIndicator = "ingredients";
            public const string DietIndicator = "diet";
            public const string TypeIndicator = "type";
            public const string CuisineIndicator = "сuisine";
        }

        public static class StepByStepMessages
        {
            public const string StepByStepInstructionIndicator = "recipe";
            public const string CookInProcessIndicator = "cookingInProcess";
            public const string NextStepIndicator = "next";
            public const string PrevStepIndicator = "prev";
            public const string CurrentStepIndicator = "current";
            public const string InstructionIndicator = "instruction";
            public const string CurrentStepNumberIndicator = "currentStepNumber";
        }

        public static class RecommendationsMessages
        {
            public const string HistoryIndicator = "history";
            public const string RandomIndicator = "random";
            public const string RecommIndicator = "recomm";
            public const string RecommendationsInProcessIndicator = "recommendationsInProcess";
        }

        public static class FavouritesMessages
        {
            public const string RemoveRecipeIndicator = "removerecipe";
            public const string AddToFavIndicator = "favrecipe";
            public const string FavIndicator = "fav";
            public const string FavListIndicator = "favouriteRecipes";
        }

        public static class HelpMessages
        {
            public const string HelpIndicator = "help";
            public const string HelpSearchIndicator = "search";
            public const string HelpStepCookIndicator = "step";
            public const string HelpRecomIndicator = "recommendation";
            public const string HelpFavIndicator = "fav";
        }
    }
}