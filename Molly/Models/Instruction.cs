﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StepByStepInstruction
{
    public class Instruction
    {
        public string name { get; set; }
        public List<Step> steps { get; set; }
    }

    public class Ingredient
    {
        public long id { get; set; }
        public string name { get; set; }
        public string image { get; set; }
    }

    public class Equipment
    {
        public long id { get; set; }
        public string name { get; set; }
        public string image { get; set; }
    }

    public class Step
    {
        public int number { get; set; }
        public string step { get; set; }
        public List<Ingredient> ingredients { get; set; }
        public List<Equipment> equipment { get; set; }
    }
}