﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Molly.Models
{
    public class ShortRecipe
    {

            public int id { get; set; }
            public string title { get; set; }
            public int readyInMinutes { get; set; }
            public string image { get; set; }
            public List<string> imageUrls { get; set; }

    }

    public class ShortRecipes
    {
        public List<ShortRecipe> recipes { get; set; }
    }
}