Here are presented time management sheets of each team member.
The main folder is divided into three sub folders. 
Each team member has own sub folder.

Each file in sub folder corresponds to one iterations and contains sheets for each week.
Each sheet contains day, date, task description and hours which are spent per day.
Also, it is calculated the total amount of hours spent per week.
